def call() {
	pipeline {		
		environment {
			git_id = 'Credentials_for_shared_lib_gitlab'
			dotnetRepo = 'http://gitlab.com/Spilchin/shadowing_project.git'
			APIKey = 'API-XUUBQVPKHMEYXPLCGZ2X1JDS4AS'
			slnfile = 'aspnet-get-started.sln'
		}
        agent {label 'window'}
        stages {
					stage('Cloning dotnet repo') {
							steps {
							cleanWs()
							git poll: true , branch: "${env.GIT_BRANCH}", credentialsId: "${git_id}", url: "${dotnetRepo}"
						}
					}
					stage('Build step') {
						steps {
							bat "nuget.exe restore ${slnfile}"
							bat "msbuild.exe ${slnfile} /t:Clean,Build /p:Configuration=Release /p:Platform=\"Any CPU\" /p:ProductVersion=1.1.1.${env.BUILD_NUMBER}"
							bat "nuget.exe spec MyProject"
							bat "nuget.exe pack MyProject.nuspec -OutputDirectory artifacats -Version 1.1.1.${env.BUILD_NUMBER} -Properties Configuration=Release"
						}
					}
					stage('Deploy') {
						steps {
							bat "octo push --package=artifacats\\Myproject.1.1.1.${BUILD_NUMBER}.nupkg --replace-existing --server http://localhost:80/ --apiKey ${APIKey}"
							bat "octo create-release --project=Deploy_sample_dotnet_web_app_to_IIS --version 0.1.${BUILD_NUMBER} --server http://localhost:80/ --apiKey ${APIKey} --packageVersion 1.1.1.${BUILD_NUMBER}"
							bat "octo deploy-release --project=Deploy_sample_dotnet_web_app_to_IIS --version 0.1.${BUILD_NUMBER} --deployto Production_IIS --server http://localhost:80/ --apiKey ${APIKey} --tenant ${env.GIT_BRANCH}"
						}
    		  }
        }
	}
}